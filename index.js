/*
	1.) Create a function that will accept the first name, last name, age, and current address (city/municipality & province) and return a complete sentence in this format: "Hello! I am Juan Dela Cruz, 21 years old, and currently living in Quezon City, Metro Manila."
*/

let person = {
	firstName: "Juan",
	lastName: "Dela Cruz",
	age: 21,
	address: {
		city: "Quezon City",
		province: "Metro Manila"
	}
}

function fullName(fName, lName, pAge, pCity,pProv){
	return "Hello I am " + fName + " " + lName + ", " + pAge + " years old. and currently living in " +pCity+", " + pProv+".";
}

let personInfo = fullName(person.firstName,person.lastName,person.age,person.address.city,person.address.province);
// let personInfo = fullName(person.firstName, person.lastName, person.age, person.address.city, person.address.province);

console.log(personInfo);

/*
	2.) Create a conditional statement that will check the age of a person and will determine if he/she is qualified to vote. If the person's age is 18 years old and above print the message "You're qualified to vote!" else print "Sorry, You're too young to vote."

	Sample Output
	//Code
	let age = 17

	//Browser Console:
	Sorry, you're too young to vote.

*/

let age = 17;

if (age > 18) {
	console.log("You're qualified to vote");
}

else {
	console.log("Sorry, you're too young to vote.");
}

/*
		3.) Create a switch case statement that will accept the input month number from a user and will print the total number of days in that month. If the input month number is not valid display an alert: "Invalid input! Please enter the month number between 1-12"


*/

let month=parseInt(prompt("Enter month number:"));
console.log(month);

switch (month) {
		case 1:
			sDays=31;
			sMonth="January";
		break;

		case 2:
			sDays=31;
			sMonth="February";
		break;

		case 3:
			sDays=31;
			sMonth="March";
		break;

		case 4:
			sDays=30;
			sMonth="April";
		break;

		case 5:
			sDays=31;
			sMonth="May";
		break;

		case 6:
			sDays=30;
			sMonth="June";
		break;

		case 7:
			sDays=31;
			sMonth="July";
		break;

		case 8:
			sDays=31;
			sMonth="August";
		break;

		case 9:
			sDays=30;
			sMonth="September";
		break;

		case 10:
			sDays=31;
			sMonth="October";
		break;

		case 11:
			sDays=30;
			sMonth="November";
		break;

		case 12:
			sDays=31;
			sMonth="December";
		break;	

		default:

		alert("Invalid Input! Please Enter between 1-12.")
}

function daysMonth(days,month){
	return "There are "+ days + " days in " + month +".";
}
let monthCalc = daysMonth(sDays,sMonth);
console.log(monthCalc);


/*
		4.) Create a function that will check if the year inputted by a user is a leap year. Using selection control structure, return the year and statement (Ex. "2016 is a leap year") if the year is a leap year. Otherwise, return a statement "Not a leap year!".

*/
let year=parseInt(prompt("Enter year to check if Leap Year"));

let yrTest1 = year%400;
let yrTest2 = year%4;
let yrTest3 = year%100;

function leapYearChecker(yrTest11,yrTest22,yrTest33){
	if(yrTest11 == 0) {
		return "Leap Year"
	}
	else if ((yrTest22 == 0) && (yrTest33 != 0)){
		return "Leap Year"
	}
	else {
		return "Not a Leap Year."
	}	

}

let resultYear = leapYearChecker(yrTest1,yrTest2,yrTest3);

console.log(resultYear);


/*
		5.) Create a function that will accept a number from the user and will count down from the given number until zero. Use a looping statement to print each number in the browser console.


*/

let loopNum = parseInt((prompt("Enter Number for Looping")));
let startNum = 0;
while (startNum <= loopNum) {
	console.log(loopNum);
	loopNum--;
}  


